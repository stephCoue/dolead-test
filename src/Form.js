import React, { Component } from "react";

class Form extends Component {
  constructor(props) {
    super(props);
    this.state = {
      valid: true
    };
  }
  handleCheckbox = event => {
    const target = event.target;
    const checked = target.checked;
    const name = target.name;

    if (this.validateForm({ [name]: checked })) {
      this.setState({
        [name]: checked
      });
    } else {
      this.setState({ valid: false, [name]: false });
    }
  };

  validateForm = nextInputObj => {
    let checked = [nextInputObj];
    for (const prop in this.state) {
      if (prop !== "valid") {
        checked.push({ [prop]: this.state[prop] });
      }
    }
    return checked.filter(input => input[Object.keys(input)[0]]).length < 3;
  };

  render() {
    const { checkbox } = this.props;
    return (
      <form className="form">
        {checkbox.map(item => (
          <Checkbox
            label={item}
            key={item}
            onPress={this.handleCheckbox}
            checked={this.state[item]}
          />
        ))}
      </form>
    );
  }
}
const Checkbox = ({ label, checked, onPress }) => (
  <label>
    <input type="checkbox" name={label} checked={checked} onChange={onPress} />
    {label}
  </label>
);

export default Form;
